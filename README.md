# Hyacinth

## 酒店民宿管理系统beta版


## 在线地址 

http://bool.yk.0zj.pw/index.php

后台账号： admin/12345   




## qq群：285067592

<div  align="center">    
  <img src="./demo/qq.png" width = "400" alt="图片名称" align=center />
</div>


## 演示
![1](./demo/登录.gif)
![1](./demo/楼层.gif)
![1](./demo/房间.gif)
![1](./demo/入住.gif)
![1](./demo/图表.gif)


![1](./demo/登录.png)

![1](./demo/首页.png)

![1](./demo/入住1.png)

![1](./demo/入住2.png)

![1](./demo/图表.png)

![1](./demo/666.png)

#-----------------------------------------------------------------------------------------------------

# 酒店系统新版本开发中

ThinkPHP 5.1版酒店管理系统
==============================
新版本对系统架构做了进一步的改进，其主要特性包括：

 + 入住管理
 + 预订管理
 + 语音提示、语音播报
 + 房态实时图表数据
 + 预警提示
 + 微信api接口
 + 短信营销
 + 会员管理
 + 酒店的商品管理
 + 报表中心
 + 财务管理
 + 设备管理。智能门锁、读卡器、身份证识别
 + 第三方平台接入：如：途牛、携程、飞猪

> 系统的运行环境要求PHP5.6以上。

## 仓库地址在线地址 

https://gitee.com/HyacinthTechnology/Hotel-System

## 部分界面展示：
![1](./demo/ru.gif)


![1](./demo/a.png)
![1](./demo/b.png)
![1](./demo/c.png)
![1](./demo/d.png)